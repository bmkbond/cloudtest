package com.clouddeploy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

    private static final Logger log = LoggerFactory.getLogger(HealthController.class);
    @Value("${health.test.msg}")
    private String healthMsg;

    @RequestMapping("/health")
    public String get() {
        log.info("Enter into /health ...");
        log.debug("healthMsg : {}",healthMsg);
        return "****** " + healthMsg+" ******";
    }
}
